﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using BLL;
using Common;
using IronJS;
using MayhemVoice.Helpers;
using VoiceModel;
using VoiceModel.CallFlow;
using WebGrease.Css.Extensions;
using States = MayhemVoice.Const.Ids;
using Txt = BLL.Enums.Text;

namespace MayhemVoice.Controllers
{
    public class MayhemController : VoiceController
    {
        private readonly CallFlow _call;
        private ServiceFcd _serviceFcd;


        public MayhemController()
        {
            _call = new CallFlow();
            _serviceFcd = new ServiceFcd();
            Init();
        }

        private void Init()
        {
        }

        public override CallFlow BuildCallFlow()
        {

            _call.SaveUserData(new UserViewModel()
            {
                AccountId = 1,
                Name = "przemek",
                SurName = "Andruszewski"
            });
            //CreateMessageFlow(States.GREETING, "first", _serviceFcd.GetTxt(Txt.Hello), true);//States.ASSIST_LOGIN TODO: Ustawic spowrotem na States.ASSIST_LOGIN
            //CreateMessageFlow(States.GREETING, States.SERVICE_READ, _serviceFcd.GetTxt(Txt.Hello), true);//States.ASSIST_LOGIN TODO: Ustawic spowrotem na States.ASSIST_LOGIN
            //CreateMessageFlow(States.GREETING, States.QUEUE_COMMAND_ACTIVE_SERVICES, _serviceFcd.GetTxt(Txt.Hello), true);//States.ASSIST_LOGIN TODO: Ustawic spowrotem na States.ASSIST_LOGIN

            CreateMessageFlow(States.GREETING, States.ASSIST_LOGIN, _serviceFcd.GetTxt(Txt.Hello), true);//States.ASSIST_LOGIN TODO: Ustawic spowrotem na States.ASSIST_LOGIN
            //CreateMessageFlow(States.GREETING, States.ASSIST_LOGIN, "Hello.", true);
            //CreateActivateServicesFlow(_call);
            //DeactivateServicesFlow(_call);
            //var servicefcd = new ServiceFcd();
            //servicefcd.AddServiceToUserServices("sms", _call.GetUserData().AccountId);




            InitMessagesFlow();


            CreateLoginFlow();


            CreateMessageFlow(States.LOGIN_SENT, States.INPUT_DATE_PROMPT, _serviceFcd.GetTxt(Txt.GoToPasswordSay));//TODO dorobic wiadomosc

            CreateDateFlow();

            CreatePasswordFlow();


            CreateMessageFlow(States.LOGGED_SUCCESSFULL, States.SERVICE_READ, _serviceFcd.GetTxt(Txt.LoggedSuccessFullInfo));//"commandSent"


            CreateServiceFlow();

            CreateServices();

            CreateMessageFlow(States.ERROR_SENDING_COMMAND, States.DO_MORE, _serviceFcd.GetTxt(Txt.ThereWasAnErrorInSendingRequest));


            var doMoreOptions = new List<string> { _serviceFcd.GetTxt(Txt.Yes), _serviceFcd.GetTxt(Txt.No) };

            _call.AddState(ViewStateBuilder.Build(States.DO_MORE,
                new Ask(States.DO_MORE, new Prompt(_serviceFcd.GetTxt(Txt.MayIAssistYouAnyting))
                {
                    bargein = false
                },
                    new Grammar(States.ASSIST_OPTIONS, doMoreOptions))
                {
                    noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearIfAssist), _serviceFcd.GetTxt(Txt.CouldNotHearIfAssist2), _serviceFcd.GetTxt(Txt.CouldNotHearIfAssist3)),
                    nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchIfAssist), _serviceFcd.GetTxt(Txt.CouldNotMatchIfAssist2), _serviceFcd.GetTxt(Txt.CouldNotMatchIfAssist3)),
                })
                .AddTransition(States.CONTINUE_ACTION, States.GOOD_BYE, new Condition(string.Format(States.COND_RESULT, _serviceFcd.GetTxt(Txt.Yes))))
                .AddTransition(States.CONTINUE_ACTION, States.ASSIST_TARGET, new Condition(string.Format(States.COND_RESULT, _serviceFcd.GetTxt(Txt.No))))
                .AddTransition(States.NO_MATCH, States.GOOD_BYE, null)
                .AddTransition(States.NO_INPUT, States.GOOD_BYE, null));


            CreateMessageFlow(States.GOOD_BYE, new Exit(States.GOOD_BYE, _serviceFcd.GetTxt(Txt.ThanYouForUsingGoodBye)));
            return _call;
        }
        /// <summary>
        /// Metoda inicjuje podsatwowe stany z wiadomosciami dla uzytkownika
        /// </summary>
        private void InitMessagesFlow()
        {
            CreateMessageFlow(States.LOGIN_NO_MATCH, States.GOOD_BYE, _serviceFcd.GetTxt(Txt.NotUnderstandLogin), false);
            CreateMessageFlow(States.ACCOUNT_DOES_NOT_EXSIST, States.ASSIST_LOGIN, _serviceFcd.GetTxt(Txt.AccountNotExsist), false);
            CreateMessageFlow(States.ACCOUNT_DISABLED, States.GOOD_BYE, _serviceFcd.GetTxt(Txt.AccountDisabled), false);
            CreateMessageFlow(States.PASSWORD_INCORRECT, States.ASSIST_LOGIN, _serviceFcd.GetTxt(Txt.PasswordIncorrect), false);
            CreateMessageFlow(States.BIRTHDATE_INCORRECT, States.ASSIST_LOGIN, _serviceFcd.GetTxt(Txt.BirthdateIncorrect), false);
            CreateMessageFlow(States.PASSWORD_NO_MATCH, States.GOOD_BYE, _serviceFcd.GetTxt(Txt.NotUnderstandPassword), false);
            CreateMessageFlow(States.SERVICE_NO_MATCH, States.GOOD_BYE, _serviceFcd.GetTxt(Txt.NotUnderstandServiceName), false);
            CreateMessageFlow(States.AMOUNT_NO_MATCH, States.GOOD_BYE, _serviceFcd.GetTxt(Txt.NotMatchServiceName), false);
            CreateMessageFlow(States.SUCCESSFULLY_CHARGED, States.SERVICE_READ,
                CreateSay(States.SUCCESSFULLY_CHARGED, _serviceFcd.GetTxt(Txt.SuccessChargedAccount), 1000));
            CreateMessageFlow(States.DID_NOT_UNDERSTAND, States.GOOD_BYE, _serviceFcd.GetTxt(Txt.NotUnderstandRequest));
        }

        #region [Funkcje tworzace flow ]

        public void CreateLoginFlow()
        {
            //stan z podaniem loginu uzytkownika
            _call.AddState(ViewStateBuilder.Build(States.ASSIST_LOGIN, States.QUEUE_COMMAND_LOGIN,
               new Ask(States.ASSIST_LOGIN, new Prompt(_serviceFcd.GetTxt(Txt.SayLogin))
               {
                   bargein = true
               },
                   CreateGrammar(BuiltinGrammar.GrammarType.digits, Const.Configuration.LOGIN_MIN_LENGTH,
                       Const.Configuration.LOGIN_MAX_LENGTH))
               {
                   noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearLoginAndRepeat), _serviceFcd.GetTxt(Txt.SayYourLoginToAuth), _serviceFcd.GetTxt(Txt.SayLoginOrCallWillBeTakenOffline)),
                   nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchLoginAndRepeat), _serviceFcd.GetTxt(Txt.CouldNotLoginMatch), _serviceFcd.GetTxt(Txt.CouldNotLoginMatchToAuth))
               })
               .AddTransition(States.NO_MATCH, States.LOGIN_NO_MATCH, null)
               .AddTransition(States.NO_INPUT, States.LOGIN_NO_MATCH, null));

            //stan z obsluga podanego w poprzednim kroku loginu uzytkownika
            _call.AddState(new State(States.QUEUE_COMMAND_LOGIN, States.LOGIN_SENT)
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cf, State state, Event e)
                {
                    string prev = cf.GetLogin(state);
                    cf.SaveLogin(state);
                    cf.ResetBirthDate();
                    cf.FireEvent(States.CONTINUE_ACTION, string.Empty);
                }));
        }

        /// <summary>
        /// Tworzy krok w flow do wprowadzenia hasła
        /// </summary>
        private void CreatePasswordFlow()
        {
            // stan w ktorym podawane jest haslo uzytkownika
            _call.AddState(ViewStateBuilder.Build(States.ASSIST_PASSWORD, States.QUEUE_COMMAND_PASSWORD,
                new Ask(States.ASSIST_LOGIN, new Prompt(_serviceFcd.GetTxt(Txt.SayYourPassword))
                {
                    bargein = false
                },
                    CreateGrammar(BuiltinGrammar.GrammarType.digits, Const.Configuration.PASSWORD_MIN_LENGTH,
                        Const.Configuration.PASSWORD_MAX_LENGTH))
                {
                    noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearPasswordAndRepeat), _serviceFcd.GetTxt(Txt.CouldNotHearPasswordAndRepeat2), _serviceFcd.GetTxt(Txt.CouldNotHearPasswordAndRepeat3)),
                    nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchPasswordAndRepeat), _serviceFcd.GetTxt(Txt.CouldNotMatchPasswordAndRepeat2), _serviceFcd.GetTxt(Txt.CouldNotMatchPasswordAndRepeat3))
                })
                .AddTransition(States.NO_MATCH, States.PASSWORD_NO_MATCH, null)
                .AddTransition(States.NO_INPUT, States.PASSWORD_NO_MATCH, null));


            //stan z w ktorym haslo uzytkownika jest obslugiwane
            _call.AddState(new State(States.QUEUE_COMMAND_PASSWORD)//,"commandSent"
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cf, State state, Event e)
                {
                    cf.SavePassword(state);
                    var status = cf.Login(state);

                    if (status == Enums.LoginStatus.Success)
                    {
                        CreateMessageFlow(States.SUCCESSFULLY_LOGGED_INFO, States.LOGGED_SUCCESSFULL, CreateSay(States.SUCCESSFULLY_LOGGED_INFO,
                            string.Format(_serviceFcd.GetTxt(Txt.WelcomeByNameAndSurName), _call.GetUserData().Name, _call.GetUserData().SurName), 1000));
                    }
                    cf.FireEvent(States.CONTINUE_ACTION, status.GetString());
                })
                .AddTransition(States.CONTINUE_ACTION, States.ACCOUNT_DOES_NOT_EXSIST, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.AccountDoesNotExsist.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.ACCOUNT_DISABLED, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.AccountDisabled.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.PASSWORD_INCORRECT, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.BadPassword.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.BIRTHDATE_INCORRECT, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.BadBirthDate.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.SUCCESSFULLY_LOGGED_INFO, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.Success.GetString())))
                );
        }

        private void CreateDateFlow()
        {

            _call.AddState(ViewStateBuilder.Build(States.INPUT_DATE_PROMPT, States.INPUT_DATE,
               new Ask(States.ASSIST_LOGIN, new Prompt(_serviceFcd.GetTxt(Txt.SayBirthDate))
               {
                   bargein = false
               }, new Grammar(new BuiltinGrammar(BuiltinGrammar.GrammarType.date))
            )
               {
                   noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearDate), _serviceFcd.GetTxt(Txt.CouldNotHearDate2), _serviceFcd.GetTxt(Txt.CouldNotHearDate3)),
                   nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchDate), _serviceFcd.GetTxt(Txt.CouldNotMatchDate2), _serviceFcd.GetTxt(Txt.CouldNotMatchDate3))
               })
               .AddTransition(States.NO_MATCH, States.LOGIN_NO_MATCH, null)
               .AddTransition(States.NO_INPUT, States.LOGIN_NO_MATCH, null));

            //stan z obsluga podanego w poprzednim kroku loginu uzytkownika
            _call.AddState(new State(States.INPUT_DATE)
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cf, State state, Event e)
                {
                    var str = state.jsonArgs;

                    var status = cf.ValidateDate();
                    switch (status)  //obsluga daty
                    {
                        case Enums.DateInput.All:
                            cf.SaveBirthDate(state.jsonArgs);
                            break;
                        case Enums.DateInput.IncorrectDay:
                            cf.SaveDay(state.jsonArgs);
                            break;
                        case Enums.DateInput.IncorrectMonth:
                            cf.SaveMonth(state.jsonArgs);
                            break;
                        case Enums.DateInput.IncorrectYear:
                            cf.SaveYear(state.jsonArgs);
                            break;
                        case Enums.DateInput.OK:
                            cf.SaveBirthDate(state.jsonArgs);
                            break;
                    }
                    status = cf.ValidateDate();  //obsluga wiadomosci po wprowadzeniu danych


                    switch (status)  //obsluga daty
                    {
                        case Enums.DateInput.All:
                            CreateMessageFlow(States.ALL_DATE_PARTS_INCORRECT, States.INPUT_DATE_PROMPT, _serviceFcd.GetTxt(Txt.AllDatePartsIncorrect), false);
                            break;
                        case Enums.DateInput.IncorrectDay:
                            CreateMessageFlow(States.INCORRECT_DAY, States.INPUT_DATE_PROMPT, _serviceFcd.GetTxt(Txt.IncorrectDay), false);
                            break;
                        case Enums.DateInput.IncorrectMonth:
                            CreateMessageFlow(States.INCORRECT_MONTH, States.INPUT_DATE_PROMPT, _serviceFcd.GetTxt(Txt.IncorrectMonth), false);
                            break;
                        case Enums.DateInput.IncorrectYear:
                            CreateMessageFlow(States.INCORRECT_YEAR, States.INPUT_DATE_PROMPT, _serviceFcd.GetTxt(Txt.IncorrectYear), false);
                            break;
                    }


                    cf.FireEvent(States.CONTINUE_ACTION, status.GetString());

                    //string prev = cf.GetLogin(state);
                    //cf.SaveLogin(state);
                    //cf.FireEvent(States.CONTINUE_ACTION, string.Empty);
                })
                .AddTransition(States.CONTINUE_ACTION, States.ALL_DATE_PARTS_INCORRECT, new Condition(string.Format(States.COND_RESULT, Enums.DateInput.All.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.INCORRECT_DAY, new Condition(string.Format(States.COND_RESULT, Enums.DateInput.IncorrectDay.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.INCORRECT_MONTH, new Condition(string.Format(States.COND_RESULT, Enums.DateInput.IncorrectMonth.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.INCORRECT_YEAR, new Condition(string.Format(States.COND_RESULT, Enums.DateInput.IncorrectYear.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.ASSIST_PASSWORD, new Condition(string.Format(States.COND_RESULT, Enums.DateInput.OK.GetString())))
                );

        }

        /// <summary>
        ///     Tworzy wiadomoosc jako jeden ze stanow aplikacji
        /// </summary>
        /// <param name="id"></param>
        /// <param name="targetId"></param>
        /// <param name="message"></param>
        /// <param name="initialState"></param>
        public void CreateMessageFlow(string id, string targetId, string message, bool initialState = false)
        {
            if (_call.States.ContainsKey(id))
                _call.States.Remove(id);

            _call.AddState(ViewStateBuilder.Build(id, targetId,
                CreateSay(id, message)), initialState);
        }

        public void CreateMessageFlow(string id, string message, bool initialState = false)
        {
            if (_call.States.ContainsKey(id))
                _call.States.Remove(id);
            _call.AddState(ViewStateBuilder.Build(id,
                CreateSay(id, message)), initialState);
        }

        public void CreateMessageFlow(string id, VoiceModel.VoiceModel message, bool initialState = false)
        {
            if (_call.States.ContainsKey(id))
                _call.States.Remove(id);
            _call.AddState(ViewStateBuilder.Build(id,
                message), initialState);
        }
        public void CreateMessageFlow(string id, string target, VoiceModel.VoiceModel message, bool initialState = false)
        {
            if (_call.States.ContainsKey(id))
                _call.States.Remove(id);
            _call.AddState(ViewStateBuilder.Build(id, target,
                message), initialState);
        }


        private void CreateServices()
        {

        }

        private void CreateServiceFlow()
        {

            var collection = _serviceFcd.ReadAllMainServices();
            var stringCollection = new List<string>();
            collection.ForEach(x => stringCollection.AddRange(x.Prompts));

            _call.AddState(ViewStateBuilder.Build(States.SERVICE_READ,
                CreateSay(States.SERVICE_READ, string.Empty))
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cf, State state, Event e)
                {
                    var str = ServiceAsks(collection);

                    //CreateMessageFlow(States.SERVICE_PROMPT, States.ASSIST_SERVICE, CreateAskSay(States.SERVICE_PROMPT, collection));
                    //cf.FireEvent(States.CONTINUE_ACTION, status.GetString());CreateAskPrompt

                    _call.AddState(ViewStateBuilder.Build(States.ASSIST_SERVICE, States.QUEUE_COMMAND_CHOOSE_SERVICE,
                new Ask(States.ASSIST_LOGIN, CreateAskPrompt(collection),
                    new Grammar(States.COMMANDS, stringCollection))
                {
                    noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearServiceChoiceAndRepeat), _serviceFcd.GetTxt(Txt.CouldNotHearServiceChoiceAndRepeat2), _serviceFcd.GetTxt(Txt.CouldNotHearServiceChoiceAndRepeat3)),
                    nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchServiceAndRepeat), _serviceFcd.GetTxt(Txt.CouldNotMatchServiceAndRepeat2), _serviceFcd.GetTxt(Txt.CouldNotMatchServiceAndRepeat3))
                })
                .AddTransition(States.NO_MATCH, States.SERVICE_NO_MATCH, null)
                .AddTransition(States.NO_INPUT, States.SERVICE_NO_MATCH, null));



                })
                //.AddTransition(States.CONTINUE_ACTION, States.SERVICE_PROMPT, null)
                .AddTransition(States.CONTINUE_ACTION, States.ASSIST_SERVICE, null)
                
                );



            //_call.AddState(ViewStateBuilder.Build(States.ASSIST_SERVICE, States.QUEUE_COMMAND_CHOOSE_SERVICE,
            //    new Ask(States.ASSIST_LOGIN, new Prompt(string.Empty),
            //        new Grammar(States.COMMANDS, stringCollection))
            //    {
            //        noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearServiceChoiceAndRepeat), _serviceFcd.GetTxt(Txt.CouldNotHearServiceChoiceAndRepeat2), _serviceFcd.GetTxt(Txt.CouldNotHearServiceChoiceAndRepeat3)),
            //        nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchServiceAndRepeat), _serviceFcd.GetTxt(Txt.CouldNotMatchServiceAndRepeat2), _serviceFcd.GetTxt(Txt.CouldNotMatchServiceAndRepeat3))
            //    })
            //    .AddTransition(States.NO_MATCH, States.SERVICE_NO_MATCH, null)
            //    .AddTransition(States.NO_INPUT, States.SERVICE_NO_MATCH, null));


            _call.AddState(new State(States.QUEUE_COMMAND_CHOOSE_SERVICE)//,"commandSent"
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cf, State state, Event e)
                {
                    var service = _serviceFcd.GetServiceByPromptName(state.jsonArgs, cf.GetUserData().AccountId);

                    switch (service.ServiceType)
                    {
                        case Enums.ServiceType.AccountBalance:
                            CreateMessageFlow(States.QUEUE_COMMAND_ACCOUNT_BALANCE, States.SERVICE_READ, CreateSay(States.QUEUE_COMMAND_ACCOUNT_BALANCE, _serviceFcd.AccountBalance(cf.GetUserData().AccountId), 700));
                            break;

                        case Enums.ServiceType.ChargeAccount:
                            //CreateMessageFlow(States.QUEUE_COMMAND_ACCOUNT_BALANCE, States.SERVICE_READ, CreateSay(States.QUEUE_COMMAND_ACCOUNT_BALANCE, servicefcd.AccountBalance(cf.GetUserData().AccountId), 700));
                            CreateChargeAccount();
                            break;

                        case Enums.ServiceType.MyServices:
                            CreateMyServicesFlow(cf);
                            break;

                        case Enums.ServiceType.Activate:
                            CreateActivateServicesFlow(cf);
                            break;

                        case Enums.ServiceType.Deactivate:
                            DeactivateServicesFlow(cf);
                            break;
                    }
                    cf.FireEvent(States.CONTINUE_ACTION, service.ServiceType.GetString());
                })
                .AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_ACCOUNT_BALANCE, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.AccountBalance.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.ASSIST_CHARGE_ACCOUNT, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.ChargeAccount.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_MY_SERVICES, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.MyServices.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_ACTIVATE_SERVICES, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.Activate.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_ACTIVE_SERVICES, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.Deactivate.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.GOOD_BYE, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.ExitApplication.GetString())))
                //.AddTransition(States.CONTINUE_ACTION, States.SUCCESSFULLY_LOGGED_INFO, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.Success.GetString())))
                );




        }

        private void CreateActivateServicesFlow(CallFlow cf)
        {
            var collection = _serviceFcd.GetUserServicesToActivate(cf.GetUserData().AccountId).ToList();
            var stringCollection = new List<string>();
            var back = collection.FirstOrDefault(x => x.ServiceType == Enums.ServiceType.ExitMenu);
            if (back != null)
                collection.Remove(back);
            collection.ForEach(x => stringCollection.AddRange(x.Prompts));

            var product =
                (from one in stringCollection
                 from two in stringCollection
                 select string.Join(" ", one, two)).ToList();
            if (back != null)
            {
                product.AddRange(back.Prompts);
                collection.Add(back);
            }
            if (product.Count() >  1)//uzytkownik moze aktywowac pewne serwisy
            {
                CreateMessageFlow(States.QUEUE_COMMAND_ACTIVATE_SERVICES, States.ASSIST_ACTIVATE_SERVICE,
                    CreateAskSay(States.QUEUE_COMMAND_ACTIVATE_SERVICES, collection));
            }
            else //brak serwisow do uruchomienia przez uzytkownika
            {
                CreateMessageFlow(States.QUEUE_COMMAND_ACTIVATE_SERVICES, States.SERVICE_READ, _serviceFcd.GetTxt(Txt.NoServiceToActivateForYou));
            }


            _call.AddState(ViewStateBuilder.Build(States.ASSIST_ACTIVATE_SERVICE, States.QUEUE_COMMAND_CHOOSE_NEW_SERVICE,
                new Ask(States.ASSIST_ACTIVATE_SERVICE, new Prompt(string.Empty),
                    new Grammar(States.COMMANDS, product.ToList())

                    )
                {
                    noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearServiceToActivate), _serviceFcd.GetTxt(Txt.CouldNotHearServiceToActivate2), _serviceFcd.GetTxt(Txt.CouldNotHearServiceToActivate3)),
                    nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.NoMatchServiceToActivate), _serviceFcd.GetTxt(Txt.NoMatchServiceToActivate2), _serviceFcd.GetTxt(Txt.NoMatchServiceToActivate3))
                })
                .AddTransition(States.NO_MATCH, States.SERVICE_NO_MATCH, null)
                .AddTransition(States.NO_INPUT, States.SERVICE_NO_MATCH, null));



            _call.AddState(new State(States.QUEUE_COMMAND_CHOOSE_NEW_SERVICE)//,"commandSent"
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cfPar, State state, Event e)
                {
                    var service = _serviceFcd.GetServiceByPromptName(state.jsonArgs, cf.GetUserData().AccountId);
                    if (service != null && service.ServiceType == Enums.ServiceType.ExitMenu)
                    {
                        cfPar.FireEvent(States.CONTINUE_ACTION, Enums.In.Back.GetString());
                    }
                    else
                    {
                        _serviceFcd.AddServiceToUserServices(state.jsonArgs, cfPar.GetUserData().AccountId);
                        CreateMessageFlow(States.ACTIVATE_SERVICE_SUCCESS, States.SERVICE_READ, _serviceFcd.GetTxt(Txt.ServiceActivatedSuccess));
                        cfPar.FireEvent(States.CONTINUE_ACTION, Enums.In.OK.GetString());
                    }
                })
                //.AddTransition(States.CONTINUE_ACTION, States.SERVICE_READ, null)
                .AddTransition(States.CONTINUE_ACTION, States.ACTIVATE_SERVICE_SUCCESS, new Condition(string.Format(States.COND_RESULT, Enums.In.OK.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.SERVICE_READ, new Condition(string.Format(States.COND_RESULT, Enums.In.Back.GetString())))
                //.AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_MY_SERVICES, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.MyServices.GetString())))
                //.AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_ACTIVATE_SERVICES, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.Activate.GetString())))
                //.AddTransition(States.CONTINUE_ACTION, States.SUCCESSFULLY_LOGGED_INFO, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.Success.GetString())))
                );

            



        }


        private void DeactivateServicesFlow(CallFlow cf)
        {
            var collection = _serviceFcd.GetUserActiveServices(cf.GetUserData().AccountId);
            var stringCollection = new List<string>();
            collection.ForEach(x => stringCollection.AddRange(x.Prompts));

            var product = (
                from one in stringCollection
                from two in stringCollection
                select string.Join(" ", one, two)).
            ToList();

            if (product.Any()) //uzytkownik ma aktywne serwisy
            {
                var service = _serviceFcd.GetServiceByServiceId(Enums.ServiceType.ExitMenu.GetInt());//pobranie serwisu do cofania z BD
                stringCollection.AddRange(service.Prompts);//dodanie do listy mozliwych odpowiedzi
                product.AddRange(service.Prompts);
                var collectionOfServices = collection.ToList();
                collectionOfServices.Add(service);
                CreateMessageFlow(States.QUEUE_COMMAND_ACTIVE_SERVICES, States.ASSIST_ACTIVE_SERVICE, CreateAskSay(States.QUEUE_COMMAND_ACTIVE_SERVICES, collectionOfServices));
            }
            else //brak aktywnych serwisow uzytkownika
            {
                CreateMessageFlow(States.QUEUE_COMMAND_ACTIVE_SERVICES, States.SERVICE_READ, _serviceFcd.GetTxt(Txt.YouHaveNoActiveServicesToDeactivate));
            }
            _call.AddState(ViewStateBuilder.Build(States.ASSIST_ACTIVE_SERVICE, States.QUEUE_COMMAND_YOUR_SERVICE,
                new Ask(States.ASSIST_ACTIVATE_SERVICE, new Prompt(string.Empty),
                    new Grammar(States.COMMANDS, product))
                {
                    noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearServiceToDeactivate), _serviceFcd.GetTxt(Txt.CouldNotHearServiceToDeactivate2), _serviceFcd.GetTxt(Txt.CouldNotHearServiceToDeactivate3)),
                    nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchServiceToDeactivate), _serviceFcd.GetTxt(Txt.CouldNotMatchServiceToDeactivate2), _serviceFcd.GetTxt(Txt.CouldNotMatchServiceToDeactivate3))
                })
                .AddTransition(States.NO_MATCH, States.SERVICE_NO_MATCH, null)
                .AddTransition(States.NO_INPUT, States.SERVICE_NO_MATCH, null));



            _call.AddState(new State(States.QUEUE_COMMAND_YOUR_SERVICE)//,"commandSent"
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cfPar, State state, Event e)
                {
                    var service = _serviceFcd.GetServiceByPromptName(state.jsonArgs, cf.GetUserData().AccountId);
                    if (service != null && service.ServiceType == Enums.ServiceType.ExitMenu)
                    {
                        cfPar.FireEvent(States.CONTINUE_ACTION, Enums.In.Back.GetString());
                    }
                    else
                    {
                        _serviceFcd.DeleteUserService(state.jsonArgs, cfPar.GetUserData().AccountId);
                        CreateMessageFlow(States.DEACTIVATE_SERVICE_SUCCESS, States.SERVICE_READ, _serviceFcd.GetTxt(Txt.DeactivateServiceSuccess));
                        cfPar.FireEvent(States.CONTINUE_ACTION, Enums.In.OK.GetString());
                    }
                })
                //.AddTransition(States.CONTINUE_ACTION, States.SERVICE_READ, null)
                .AddTransition(States.CONTINUE_ACTION, States.DEACTIVATE_SERVICE_SUCCESS, new Condition(string.Format(States.COND_RESULT, Enums.In.OK.GetString())))
                .AddTransition(States.CONTINUE_ACTION, States.SERVICE_READ, new Condition(string.Format(States.COND_RESULT, Enums.In.Back.GetString())))
                //.AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_MY_SERVICES, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.MyServices.GetString())))
                //.AddTransition(States.CONTINUE_ACTION, States.QUEUE_COMMAND_ACTIVATE_SERVICES, new Condition(string.Format(States.COND_RESULT, Enums.ServiceType.Activate.GetString())))
                //.AddTransition(States.CONTINUE_ACTION, States.SUCCESSFULLY_LOGGED_INFO, new Condition(string.Format(States.COND_RESULT, Enums.LoginStatus.Success.GetString())))
                );




        }

        private void CreateMyServicesFlow(CallFlow cf)
        {
            var collection = _serviceFcd.GetUserActiveServices(cf.GetUserData().AccountId);
            if (collection == null || !collection.Any())
            {
                CreateMessageFlow(States.QUEUE_COMMAND_MY_SERVICES, States.SERVICE_READ, _serviceFcd.GetTxt(Txt.YouHaveNoActiveServices));
                return;
            }
            CreateMessageFlow(States.QUEUE_COMMAND_MY_SERVICES, States.SERVICE_READ, CreateSay(States.QUEUE_COMMAND_MY_SERVICES, collection));
        }


        private void CreateChargeAccount()
        {
            _call.AddState(ViewStateBuilder.Build(States.ASSIST_CHARGE_ACCOUNT, States.QUEUE_COMMAND_CHARGE_ACCOUNT,
               new Ask(States.ASSIST_CHARGE_ACCOUNT, new Prompt(_serviceFcd.GetTxt(Txt.HowMuchChargeAsk))
               {
                   bargein = false
               },
                   CreateGrammar(BuiltinGrammar.GrammarType.currency, Const.Configuration.CURRENCY_MIN_LENGTH,
                       Const.Configuration.CURRENCY_MAX_LENGTH))
               {
                   noinputPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotHearChargeAccount), _serviceFcd.GetTxt(Txt.CouldNotHearChargeAccount2), _serviceFcd.GetTxt(Txt.CouldNotHearChargeAccount3)),
                   nomatchPrompts = GetPrompts(_serviceFcd.GetTxt(Txt.CouldNotMatchChargeAccount), _serviceFcd.GetTxt(Txt.CouldNotMatchChargeAccount2), _serviceFcd.GetTxt(Txt.CouldNotMatchChargeAccount3))
               })
               .AddTransition(States.NO_MATCH, States.AMOUNT_NO_MATCH, null)
               .AddTransition(States.NO_INPUT, States.AMOUNT_NO_MATCH, null));


            _call.AddState(new State(States.QUEUE_COMMAND_CHARGE_ACCOUNT)//,"commandSent"
                .AddTransition(States.ERROR, States.ERROR_SENDING_COMMAND, null)
                .AddOnEntryAction(delegate(CallFlow cf, State state, Event e)
                {
                    var val = state.jsonArgs;
                    decimal currency;
                    decimal.TryParse(val.ToUpper().Replace("USD", string.Empty).Replace('.', ','), out currency);

                    if (currency > int.Parse(_serviceFcd.GetTxt(Txt.MinimumCurrencyToCharge)))
                    {
                        _serviceFcd.ChargeAccount(cf.GetUserData().AccountId, currency);
                        cf.FireEvent(States.CONTINUE_ACTION, true.ToString());
                    }
                    else
                    {
                        CreateMessageFlow(States.MINIMUM_CHARGE_CURRENCY_TO_LOW, States.ASSIST_CHARGE_ACCOUNT, string.Format(_serviceFcd.GetTxt(Txt.CurrencyIsTooLowInfo), _serviceFcd.GetTxt(Txt.MinimumCurrencyToCharge)));
                        cf.FireEvent(States.CONTINUE_ACTION, false.ToString());
                    }


                })
                //.AddTransition(States.CONTINUE_ACTION, States.SUCCESSFULLY_CHARGED, null)
                .AddTransition(States.CONTINUE_ACTION, States.SUCCESSFULLY_CHARGED, new Condition(string.Format(States.COND_RESULT, true.ToString())))
                .AddTransition(States.CONTINUE_ACTION, States.MINIMUM_CHARGE_CURRENCY_TO_LOW, new Condition(string.Format(States.COND_RESULT, false.ToString())))
                );
        }


        #endregion [Funkcje tworzace flow ]

        #region [ Funkcje pomocnicze ]

        /// <summary>
        ///     Funkcja tworzy prompt
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private Prompt CreatePrompt(string message)
        {
            return new Prompt(message)
            {
                bargein = false
            };
        }

        private Prompt CreatePrompt(string message, int miliseconds)
        {
            var prompt = new Prompt()
            {
                bargein = false
            };
            prompt.audios.Add(new TtsMessage(message));
            prompt.audios.Add(new Silence(miliseconds));
            return prompt;
        }

        private Prompt CreateAskPrompt(IEnumerable<IAsk> collection)
        {
            int orOptionTime = Const.Configuration.TIME_BETWEEN_NEXT_OPTION_SERVICE;
            var prompt = new Prompt()
            {
                bargein = true
            };
            if (collection != null)
            {

                foreach (var item in collection)
                {
                    string str = string.Format("for {0} say: ", item.Name);
                    prompt.audios.Add(new TtsMessage(str));
                    prompt.audios.Add(new Silence(orOptionTime));
                    var first = item.Prompts.FirstOrDefault();
                    if (first != null)
                    {
                        str = first;
                        prompt.audios.Add(new TtsMessage(str));
                        prompt.audios.Add(new Silence(orOptionTime));
                        for (int i = 1; i < item.Prompts.Count(); i++)
                        {
                            str = " or " + item.Prompts[i];
                            prompt.audios.Add(new TtsMessage(str));
                            prompt.audios.Add(new Silence(orOptionTime));
                        }
                    }
                }
            }
            return prompt;
        }


        private Prompt CreateSayPrompt(IEnumerable<IAsk> collection)
        {
            int orOptionTime = Const.Configuration.TIME_BETWEEN_NEXT_OPTION_SERVICE;
            var prompt = new Prompt()
            {
                bargein = true
            };
            if (collection != null)
            {
                prompt.audios.Add(new TtsMessage("You have"));
                foreach (var item in collection)
                {
                    string str = string.Format("{0}", item.Name);
                    prompt.audios.Add(new TtsMessage(str));
                    prompt.audios.Add(new Silence(orOptionTime));
                   
                }
            }
            return prompt;
        }

        private Say CreateSay(string id, string message)
        {
            return new Say(id, CreatePrompt(message));
        }
        private Say CreateSay(string id, string message, int miliseconds)
        {
            return new Say(id, CreatePrompt(message, miliseconds));
        }

        private Say CreateAskSay(string id, IEnumerable<IAsk> collection)
        {
            return new Say(id, CreateAskPrompt(collection));
        }

        private Say CreateSay(string id, IEnumerable<IAsk> collection)
        {
            return new Say(id, CreateSayPrompt(collection));
        }

        private Grammar CreateGrammar(BuiltinGrammar.GrammarType grammar, int minLength, int maxLength)
        {
            var numberVerificateGrammar = new BuiltinGrammar(grammar, minLength, maxLength);
            return new Grammar(numberVerificateGrammar);
        }

        public List<Prompt> GetPrompts(int count, string msg)
        {
            var collection = new List<Prompt>();
            var assistNomatch = CreatePrompt(msg);
            for (int i = 0; i < count; i++)
            {
                collection.Add(assistNomatch);
            }

            return collection;
        }

        public List<Prompt> GetPrompts(params string[] msg)
        {
            var collection = new List<Prompt>();

            msg.ForEach(x =>
            {
                var assistNomatch = CreatePrompt(x);
                collection.Add(assistNomatch);
            });

            return collection;
        }

        private string ServiceAsks(IEnumerable<IAsk> coll)
        {
            return string.Join(" ", coll.Select(x => x.Text));
        }

        #endregion [ Funkcje pomocnicze ]
    }
}