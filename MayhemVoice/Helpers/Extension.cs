﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;
using Common;
using VoiceModel.CallFlow;
using States = MayhemVoice.Const.Ids;
namespace MayhemVoice.Helpers
{
    public static class Extension
    {
        public static void SavePassword(this CallFlow cf, State state)
        {
            var fcd = new ServiceFcd();
            fcd.RegisterLogActivity(state.jsonArgs, Enums.LogType.Password);
            cf.Ctx.SetGlobal(States.USER_PASSWORD, state.jsonArgs);
        }

        public static void SaveLogin(this CallFlow cf, State state)
        {
            var fcd = new ServiceFcd();
            fcd.RegisterLogActivity(state.jsonArgs, Enums.LogType.Login);
            cf.Ctx.SetGlobal(States.USER_LOGIN, state.jsonArgs);
        }

        public static void SaveUserId(this CallFlow cf, int id)
        {
            cf.Ctx.SetGlobal(States.USER_ID, id);
        }

        public static string GetLogin(this CallFlow cf, State state)
        {
            var login = cf.Ctx.GetGlobal(States.USER_LOGIN);
            if (login.IsString)
                return login.String;
            return string.Empty;
        }

        public static string GetPassword(this CallFlow cf, State state)
        {
            var login = cf.Ctx.GetGlobal(States.USER_PASSWORD);
            if (login.IsString)
                return login.String;
            return string.Empty;
        }

        public static int GetUserId(this CallFlow cf, State state)
        {
            var login = cf.Ctx.GetGlobal(States.USER_ID);
            if (login.IsNumber)
                return (int)login.Number;
            return 0;
        }
        public static void SaveUserData(this CallFlow cf, UserViewModel model)
        {
            cf.Ctx.SetGlobal(States.USER_DATA, model);
        }

        public static UserViewModel GetUserData(this CallFlow cf)
        {
            var obj = cf.Ctx.GetGlobalAs<UserViewModel>(States.USER_DATA);

            return obj;
        }

        public static Enums.LoginStatus Login(this CallFlow cf, State state)
        {
            string login = cf.GetLogin(state);
            string pass = cf.GetPassword(state);
            string year = cf.Ctx.GetGlobal("year").String;
            string month = cf.Ctx.GetGlobal("month").String;
            string day = cf.Ctx.GetGlobal("day").String;

            var loginFcd = new LoginFcd();
            var result = loginFcd.Login(login, pass, new DateTime(int.Parse(year), int.Parse(month), int.Parse(day)));

            if (result.Item1 == Enums.LoginStatus.Success)
            {
                // zalogowany poprawnie
                cf.SaveUserId(result.Item2);
                cf.SaveUserData(loginFcd.GetUserModel(result.Item2));
            }
            return result.Item1;
        }

        public static Enums.DateInput SaveBirthDate(this CallFlow cf, string data)
        {
            cf.SaveYear(data);
            cf.SaveMonth(data);
            cf.SaveDay(data);
            var fcd = new ServiceFcd();
            fcd.RegisterLogActivity(data, Enums.LogType.BirthDate);
            return cf.ValidateDate();
        }

        private static void SaveData(this CallFlow cf, string key, string data)
        {
            cf.Ctx.SetGlobal(key, data);
        }

        public static Enums.DateInput ValidateDate(this CallFlow cf)
        {
            if (
                !cf.IsValidSessionDate("year")
                && !cf.IsValidSessionDate("month")
                && !cf.IsValidSessionDate("day")
                )
                return Enums.DateInput.All;

            if (!cf.IsValidSessionDate("year"))
                return Enums.DateInput.IncorrectYear;

            if (!cf.IsValidSessionDate("month"))
                return Enums.DateInput.IncorrectMonth;

            if (!cf.IsValidSessionDate("day"))
                return Enums.DateInput.IncorrectDay;

            return Enums.DateInput.OK;
        }

        public static void SaveYear(this CallFlow cf, string data)
        {
            var year = data.Substring(0, 4);
            cf.SaveData("year", year);      //year
        }

        public static void SaveMonth(this CallFlow cf, string data)
        {
            var month = data.Substring(4, 2);
            cf.SaveData("month", month);    //month
        }

        public static void SaveDay(this CallFlow cf, string data)
        {
            var day = data.Substring(6, 2);
            cf.SaveData("day", day);        //day
        }

        private static bool IsValidSessionDate(this CallFlow cf, string key)
        {
            var val = cf.Ctx.GetGlobal(key).IsString ? cf.Ctx.GetGlobal(key).String : "?";
            return IsDateValid(val);
        }
        public static void ResetBirthDate(this CallFlow cf)
        {
            cf.SaveData("year", "?");       //year
            cf.SaveData("month", "?");      //month
            cf.SaveData("day", "?");        //day
        }
        private static bool IsDateValid(string data)
        {
            return !data.StartsWith("?");
        }
    }
}