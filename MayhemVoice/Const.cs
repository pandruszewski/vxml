﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MayhemVoice
{
    public class Const
    {
        public static class Configuration
        {
            public static readonly int LOGIN_MIN_LENGTH = 0;
            public static readonly int LOGIN_MAX_LENGTH = 5;
            public static readonly int PASSWORD_MIN_LENGTH = 0;
            public static readonly int PASSWORD_MAX_LENGTH = 5;
            public static readonly int REPEAT_COUNT_NO_INPUT = 2;
            public static readonly int REPEAT_COUNT_NO_MATCH = 2;
            public static readonly int TIME_BETWEEN_NEXT_OPTION_SERVICE = 500;
            public static readonly int CURRENCY_MIN_LENGTH = 0;
            public static readonly int CURRENCY_MAX_LENGTH = 10;
        }

        public static class Ids
        {
            public static readonly string GREETING = "greeting";
            public static readonly string ASSIST_LOGIN = "assist";
            public static readonly string ASSIST_PASSWORD = "assistpassword";
            public static readonly string QUEUE_COMMAND_LOGIN = "queuecommand";
            public static readonly string QUEUE_COMMAND_PASSWORD = "queuecommandpassword";
            public static readonly string USER_LOGIN = "userlogin";
            public static readonly string USER_PASSWORD = "userpassword";
            public static readonly string USER_DATA = "userdata";
            public static readonly string USER_ID = "userid";
            public static readonly string LOGIN_SENT = "loginsent";
            public static readonly string ACCOUNT_DOES_NOT_EXSIST = "accountdoesnotexsist";
            public static readonly string ACCOUNT_DISABLED = "accountdisabled";
            public static readonly string PASSWORD_INCORRECT = "passwordincorrect";
            public static readonly string PASSWORD_NO_MATCH = "passwordnomatch";
            public static readonly string LOGIN_NO_MATCH = "loginnomatch";
            public static readonly string GOOD_BYE = "goodbye";
            public static readonly string SUCCESSFULLY_LOGGED_INFO = "greetinguserwithname";
            public static readonly string SERVICE_READ = "serviceread";
            public static readonly string SERVICE_PROMPT = "serviceprompt";
            public static readonly string ASSIST_SERVICE = "assistservice";
            public static readonly string QUEUE_COMMAND_CHOOSE_SERVICE = "chooseservice";
            public static readonly string QUEUE_COMMAND_ACCOUNT_BALANCE = "accountbalance";
            public static readonly string CHARGE_ACCOUNT = "chargeaccount";
            public static readonly string ASSIST_CHARGE_ACCOUNT = "assistchargeaccount";
            public static readonly string QUEUE_COMMAND_CHARGE_ACCOUNT = "commandchargeaccount";
            public static readonly string SUCCESSFULLY_CHARGED = "sucessfullycharged";
            public static readonly string QUEUE_COMMAND_MY_SERVICES = "queuecommandmyservices";
            public static readonly string QUEUE_COMMAND_ACTIVATE_SERVICES = "queueactivateservices";
            public static readonly string ASSIST_ACTIVATE_SERVICE = "assistactivateservice";
            public static readonly string QUEUE_COMMAND_CHOOSE_NEW_SERVICE = "queuecommandchoosenewservice";
            public static readonly string QUEUE_COMMAND_ACTIVE_SERVICES = "activeservices";
            public static readonly string ASSIST_ACTIVE_SERVICE = "assistactiveservices";
            public static readonly string QUEUE_COMMAND_YOUR_SERVICE = "yourservices";
            public static readonly string LOGGED_SUCCESSFULL = "loggedSuccessfull";
            public static readonly string SERVICE_NO_MATCH = "serviceNoMatch";
            public static readonly string AMOUNT_NO_MATCH = "amountNoMatch";
            public static readonly string MINIMUM_CHARGE_CURRENCY_TO_LOW = "minimumCurrencyTooLow";
            public static readonly string NO_MATCH = "nomatch";
            public static readonly string NO_INPUT ="noinput";
            public static readonly string DO_MORE ="doMore";
            public static readonly string ERROR_SENDING_COMMAND ="errSendingCommand";
            public static readonly string ASSIST_OPTIONS ="assistOptions";
            public static readonly string CONTINUE_ACTION ="continue";
            public static readonly string ASSIST_TARGET ="assist";
            public static readonly string DID_NOT_UNDERSTAND ="didNotUnderstand";
            public static readonly string ERROR ="error";
            public static readonly string COMMANDS ="commands";
            public static readonly string COND_RESULT ="result == '{0}'";
            public static readonly string ALL_DATE_PARTS_INCORRECT = "alldatepartsincorrect";
            public static readonly string INCORRECT_DAY = "incorrectDay";
            public static readonly string INCORRECT_MONTH = "incorrectMonth";
            public static readonly string INCORRECT_YEAR = "incorrectYear";
            public static readonly string INPUT_DATE_PROMPT = "inputDatePrompt";
            public static readonly string INPUT_DATE = "inputDate";
            public static readonly string BIRTHDATE_INCORRECT = "birthdateIncorrect";
            public static readonly string ACTIVATE_SERVICE_SUCCESS = "activateServiceSuccess";
            public static readonly string DEACTIVATE_SERVICE_SUCCESS = "deactivateServicessSuccess";
        }
    }
}