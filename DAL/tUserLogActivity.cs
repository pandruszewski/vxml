//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tUserLogActivity
    {
        public int UserLogActivityId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string LoggedText { get; set; }
        public Nullable<int> TypeStatusDictId { get; set; }
    
        public virtual tAccount tAccount { get; set; }
    }
}
