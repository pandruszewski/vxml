﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BLL;
using Common;

namespace DAL
{
    public class LoginDAO
    {
        public tAccount FindUserByLogin(string login)
        {
            using (var ctx = new VoxeoEntities())
            {
                var account = ctx.tAccount.FirstOrDefault(x => x.Login == login);
                return account;
            }
        }

        private string GetEncryptedString(string pass)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(pass);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            return System.Text.Encoding.ASCII.GetString(data);
        }

        public void AddLoginHistory(int accountId, Enums.LoginStatus loginStatus)
        {
            var incorrectLogin = new tAccountLoginHistory() { AccountId = accountId, LoginStatus = loginStatus.GetInt(), LoginUTCDate = DateTime.UtcNow };
            using (var ctx = new VoxeoEntities())
            {
                var account = ctx.tAccount.Single(x => x.AccountId == accountId);
                if (!(account.AccountDisableUTCDate > DateTime.UtcNow))
                {
                    ctx.tAccountLoginHistory.Add(incorrectLogin);
                    ctx.SaveChanges();
                    int blockTime = 5;
                    var dt = DateTime.UtcNow.AddMinutes(-blockTime);
                    var badPasswordStatus = Enums.LoginStatus.BadPassword.GetInt();
                    var isDisabled = ctx.tAccountLoginHistory.Count(
                        x => x.AccountId == accountId && x.LoginStatus == badPasswordStatus && x.LoginUTCDate > dt) > 2;


                    if (isDisabled)
                    {
                        account.AccountDisableUTCDate = DateTime.UtcNow.AddMinutes(blockTime);
                        ctx.tAccount.AddOrUpdate(account);
                        ctx.SaveChanges();
                    }
                }
            }
        }


        public UserViewModel GetUserModel(int accountId)
        {
            using (var ctx = new VoxeoEntities())
            {
                var account = ctx.tAccount.Where(x => x.AccountId == accountId).Select(x => new UserViewModel() { AccountId = x.AccountId, Name = x.Name, SurName = x.SurName }).First();
                return account;
            }
        }
    }
}
