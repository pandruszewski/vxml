﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using BLL;
using Common;

namespace DAL
{
    public class ServiceDAO
    {
        public List<ServiceViewModel> GetAllMainServices()
        {
            using (var ctx = new VoxeoEntities())
            {
                var coll = ctx.tService.Where(x => x.IsMain == true).ToList().Select(x => ConvertToServiceViewModel(x)).ToList();
                return coll;
            }
        }

        public ServiceViewModel GetServiceByPromptName(string promptTxt)
        {
            using (var ctx = new VoxeoEntities())
            {
                return ctx.tServicePrompt.Where(x => x.Prompt.Contains(promptTxt)).ToList()
                     .Select(x => ConvertToServiceViewModel(x.tService))
                     .FirstOrDefault();
            }
        }

        private ServiceViewModel ConvertToServiceViewModel(tService x)
        {
            return new ServiceViewModel()
            {
                Name = x.ServiceName,
                ServiceType = (Enums.ServiceType)x.ServiceId,
                Prompts = x.tServicePrompt.Select(z => z.Prompt).ToList()
            };
        }

        public int AccountBalance(int accountId)
        {
            using (var ctx = new VoxeoEntities())
            {
                var val = ctx.tAccount.First(x => x.AccountId == accountId).Balance;
                return val.HasValue ? val.Value : 0;
            }
        }

        public void ChargeAccount(int userId, decimal currency)
        {
            int val = Convert.ToInt32(currency);
            using (var ctx = new VoxeoEntities())
            {
                var user = ctx.tAccount.First(x => x.AccountId == userId);
                user.Balance += val;
                ctx.tAccount.AddOrUpdate(user);
                ctx.SaveChanges();
            }
        }

        public IEnumerable<ServiceViewModel> GetUserActiveServices(int accountId)
        {
            using (var ctx = new VoxeoEntities())
            {
                var val = ctx.tAccount.Where(x => x.AccountId == accountId).Select(x => x.tService).FirstOrDefault().Select(ConvertToServiceViewModel).ToList();

                if (val == null || !val.Any())
                    return null;
                return val;
            }
        }

        public IEnumerable<ServiceViewModel> GetServicesToActivatee(int accountId)
        {
            using (var ctx = new VoxeoEntities())
            {
                var servicies = ctx.uspReadUserReadyToActivateServices(accountId).ToList().Select(x =>
                    new ServiceViewModel()
                    {
                        Name = x.ServiceName,
                        ServiceType = (Enums.ServiceType)x.ServiceId,
                        Prompts = x.prompt.Split(';').Where(z => !string.IsNullOrEmpty(z)).ToList()
                    }
                    );


                return servicies;
            }
        }

        public void AddServiceToUserServices(string jsonArgs, int accountId)
        {
            using (var ctx = new VoxeoEntities())
            {
                var servicePrompt = ctx.tServicePrompt.First(x => x.Prompt.Equals(jsonArgs, StringComparison.InvariantCulture));
                var account = ctx.tAccount.First(x => x.AccountId == accountId);
                if (account.tService.FirstOrDefault(x => x.ServiceId == servicePrompt.ServiceId) == null)//uzytkownik nie ma aktywnego tego serwisu
                {
                    account.tService.Add(servicePrompt.tService);
                    ctx.SaveChanges();
                }
            }
        }

        public void DeleteUserService(string jsonArgs, int accountId)
        {
            using (var ctx = new VoxeoEntities())
            {
                var servicePrompt = ctx.tServicePrompt.First(x => x.Prompt.Equals(jsonArgs, StringComparison.InvariantCulture));
                var account = ctx.tAccount.First(x => x.AccountId == accountId);
                account.tService.Remove(servicePrompt.tService);
                ctx.SaveChanges();
            }   
        }

        public ServiceViewModel GetServiceByServiceId(int serviceId)
        {
            using (var ctx = new VoxeoEntities())
            {
                return ctx.tService.Where(x => x.ServiceId == serviceId)
                    .ToList()
                    .Select(ConvertToServiceViewModel)
                    .FirstOrDefault();
            }
        }

        public void LogUserActivity(string txt, int accountId, Enums.LogType type)
        {
            using (var ctx = new VoxeoEntities())
            {
                int? accId = null;
                if (accountId > 0)
                    accId = accountId;
      
                var log = new tUserLogActivity()
                {
                    AccountId = accId,
                    LoggedText = txt,
                    TypeStatusDictId = type.GetInt()
                };
                ctx.tUserLogActivity.Add(log);
                ctx.SaveChanges();
            }
        }

        public List<tDictionary> GetDictionaries()
        {
            using (var ctx = new VoxeoEntities())
            {
                return ctx.tDictionary.ToList();
            }
        }
    }
}
