﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class UserViewModel
    {
        public string Name { get; set; }

        public string SurName { get; set; }

        public int AccountId { get; set; }


        public string UserDataGreeting
        {
            get { return string.Format("Hello {0} {1}", Name, SurName); }
        }
    }
}
