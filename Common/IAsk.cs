﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public interface IAsk
    {
        string Name { get; set; }

        List<string> Prompts { get; set; }

        string Text { get; }
    }
}
