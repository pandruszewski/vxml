﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;

namespace Common
{
    public class ServiceViewModel : IAsk
    {
        public Enums.ServiceType ServiceType { get; set; }

        public string Name { get; set; }

        public List<string> Prompts { get; set; }


        public string Text
        {
            get
            {
                string str = string.Format("for {0} say: ", Name);
                var first = Prompts.FirstOrDefault();
                if (first != null)
                {
                    str += first;
                    for (int i = 1; i < Prompts.Count(); i++)
                    {
                        str += " or " + Prompts[i];
                    }
                }
                return str;

            }
        }
    }
}
