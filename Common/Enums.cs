﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class Enums
    {
        public enum LoginStatus
        {
            AccountDoesNotExsist = 1,
            BadPassword = 2,
            AccountDisabled = 3,
            Success = 4,
            BadRequest = 5,
            BadBirthDate = 6,
        }


        public enum ServiceType
        {
            AccountBalance = 1,
            ChargeAccount = 2,
            MyServices = 3,
            Activate = 6,
            Deactivate = 7,
            ExitApplication = 8,
            ExitMenu = 9,
        }

        public enum In
        {
            OK = 1,
            Back = 2,
        }

        public enum LogType
        {
            SearchService = 1,
            ChargeAccount = 2,
            AddUserService = 3,
            DeleteUserService = 4,
            Login = 5,
            Password = 6,
            BirthDate = 7,
        }

        public enum Text
        {
            Hello = 1,
            NotUnderstandLogin = 2,
            AccountNotExsist = 3,
            AccountDisabled = 4,
            PasswordIncorrect = 5,
            NotUnderstandPassword = 6,
            NotUnderstandServiceName = 7,
            NotMatchServiceName = 8,
            SuccessChargedAccount = 9,
            NotUnderstandRequest = 10,
            SayLogin = 11,
            CouldNotHearLoginAndRepeat = 12,
            CouldNotMatchLoginAndRepeat = 13,
            GoToPasswordSay = 14,
            SayYourPassword = 15,
            CouldNotHearPasswordAndRepeat = 16,
            CouldNotMatchPasswordAndRepeat = 17,
            LoggedSuccessFullInfo = 18,
            CouldNotHearServiceChoiceAndRepeat = 19,
            CouldNotMatchServiceAndRepeat = 20,
            HowMuchChargeAsk = 21,
            CouldNotHearChargeAccount = 22,
            CouldNotMatchChargeAccount = 23,
            CurrencyIsTooLowInfo = 24,
            MinimumCurrencyToCharge = 25,
            YouHaveNoActiveServices = 26,
            NoServiceToActivateForYou = 27,
            CouldNotHearServiceToActivate = 28,
            NoMatchServiceToActivate = 29,
            YouHaveNoActiveServicesToDeactivate = 30,
            CouldNotHearServiceToDeactivate = 31,
            CouldNotMatchServiceToDeactivate = 32,
            ThereWasAnErrorInSendingRequest = 33,
            Yes = 34,
            No = 35,
            MayIAssistYouAnyting = 36,
            CouldNotHearIfAssist = 37,
            CouldNotMatchIfAssist = 38,
            ThanYouForUsingGoodBye = 39,
            AllDatePartsIncorrect = 40,
            IncorrectDay = 41,
            IncorrectMonth = 42,
            IncorrectYear = 43,
            SayBirthDate = 44,
            CouldNotHearDate = 45,
            CouldNotMatchDate = 46,
            BirthdateIncorrect = 47,
            WelcomeByNameAndSurName = 48,
            SayYourLoginToAuth = 49,
            SayLoginOrCallWillBeTakenOffline = 50,
            CouldNotLoginMatch = 51,
            CouldNotLoginMatchToAuth = 52,
            CouldNotHearIfAssist2 = 53,
            CouldNotHearIfAssist3 = 54,
            CouldNotMatchIfAssist2 = 55,
            CouldNotMatchIfAssist3 = 56,
            CouldNotHearPasswordAndRepeat2 = 57,
            CouldNotHearPasswordAndRepeat3 = 58,
            CouldNotMatchPasswordAndRepeat2 = 59,
            CouldNotMatchPasswordAndRepeat3 = 60,
            CouldNotHearDate2 = 61,
            CouldNotHearDate3 = 62,
            CouldNotMatchDate2 = 63,
            CouldNotMatchDate3 = 64,
            CouldNotHearServiceChoiceAndRepeat2 = 65,
            CouldNotHearServiceChoiceAndRepeat3 = 66,
            CouldNotMatchServiceAndRepeat2 = 67,
            CouldNotMatchServiceAndRepeat3 = 68,
            CouldNotHearServiceToActivate2 = 69,
            CouldNotHearServiceToActivate3 = 70,
            NoMatchServiceToActivate2 = 71,
            NoMatchServiceToActivate3 = 72,
            CouldNotHearServiceToDeactivate2 = 73,
            CouldNotHearServiceToDeactivate3 = 74,
            CouldNotMatchServiceToDeactivate2 = 75,
            CouldNotMatchServiceToDeactivate3 = 76,
            CouldNotHearChargeAccount2 = 77,
            CouldNotHearChargeAccount3 = 78,
            CouldNotMatchChargeAccount2 = 79,
            CouldNotMatchChargeAccount3 = 80,
            ServiceActivatedSuccess = 81,
            DeactivateServiceSuccess = 82,
        }

        public enum DateInput
        {
        OK = 1,
            IncorrectYear = 2,
            IncorrectMonth = 3,
            IncorrectDay = 4,
            All = 5,
        }

        public static string GetString(this Enum status)
        {
            return status.GetInt().ToString(CultureInfo.InvariantCulture);
        }

        public static int GetInt(this Enum status)
        {
            return Convert.ToInt32(status);
        }

    }
}
