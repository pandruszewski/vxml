﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DAL;

namespace BLL
{
    public class ServiceFcd
    {
        private ServiceDAO _serviceDao;
        private List<tDictionary> dictionaries;

        public ServiceFcd()
        {
            _serviceDao = new ServiceDAO();
        }
        public IEnumerable<ServiceViewModel> ReadAllMainServices()
        {
            var collection = _serviceDao.GetAllMainServices();
            return collection;
        }

        public ServiceViewModel GetServiceByPromptName(string promptName, int accountId)
        {
            _serviceDao.LogUserActivity(promptName, accountId, Enums.LogType.SearchService);
            return _serviceDao.GetServiceByPromptName(promptName);
        }

        public string AccountBalance(int accountId)
        {
            return string.Format("You Have {0} points.", _serviceDao.AccountBalance(accountId));
        }

        public void ChargeAccount(int userId, decimal currency)
        {
            _serviceDao.LogUserActivity(currency.ToString(), userId, Enums.LogType.ChargeAccount);
            _serviceDao.ChargeAccount(userId, currency);
        }

        public IEnumerable<ServiceViewModel> GetUserActiveServices(int accountId)
        {
            return _serviceDao.GetUserActiveServices(accountId);
        }

        public IEnumerable<ServiceViewModel> GetUserServicesToActivate(int accountId)
        {
            return _serviceDao.GetServicesToActivatee(accountId);
        }

        public void AddServiceToUserServices(string jsonArgs, int accountId)
        {
            var services = jsonArgs.Split().Distinct().ToList();
            services.ForEach(x =>
            {
                _serviceDao.LogUserActivity(x, accountId, Enums.LogType.AddUserService);
                _serviceDao.AddServiceToUserServices(x, accountId);
            });

        }

        public void DeleteUserService(string jsonArgs, int accountId)
        {
            var services = jsonArgs.Split().Distinct().ToList();
            services.ForEach(x =>
            {
                _serviceDao.LogUserActivity(x, accountId, Enums.LogType.DeleteUserService);
                _serviceDao.DeleteUserService(x, accountId);
            });

        }

        public ServiceViewModel GetServiceByServiceId(int serviceId)
        {
            return _serviceDao.GetServiceByServiceId(serviceId);
        }

        public string GetTxt(Enums.Text val)
        {
            if (dictionaries == null)
                dictionaries = _serviceDao.GetDictionaries();

            var dictId = val.GetInt();

            var dict = dictionaries.FirstOrDefault(x => x.DictionaryId == dictId);

            if (dict == null)
                return string.Empty;

            return dict.Text;
        }

        public void RegisterLogActivity(string txt, Enums.LogType type)
        {
            _serviceDao.LogUserActivity(txt, 0, type);
        }

    }
}
