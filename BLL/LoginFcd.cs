﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BLL
{
    public class LoginFcd
    {
        private LoginDAO _loginDAO;

        public LoginFcd()
        {
            _loginDAO = new LoginDAO();
        }
        public Tuple<Enums.LoginStatus, int> Login(string login, string password, DateTime dt)
        {
            var user = _loginDAO.FindUserByLogin(login);

            if (user == null)
                return GetStatusWithAccount(Enums.LoginStatus.AccountDoesNotExsist, 0);

            if (user.Password.Trim() != password)
            {
                _loginDAO.AddLoginHistory(user.AccountId, Enums.LoginStatus.BadPassword);
                return GetStatusWithAccount(Enums.LoginStatus.BadPassword, 0);
            }

            if (user.AccountDisableUTCDate.HasValue && user.AccountDisableUTCDate > DateTime.UtcNow)
                return GetStatusWithAccount(Enums.LoginStatus.AccountDisabled, 0);

            if (user.BirthDate.HasValue && user.BirthDate.Value != dt.Date)
                return GetStatusWithAccount(Enums.LoginStatus.BadBirthDate, 0);

            _loginDAO.AddLoginHistory(user.AccountId, Enums.LoginStatus.Success);
            return GetStatusWithAccount(Enums.LoginStatus.Success, user.AccountId);

        }

        private Tuple<Enums.LoginStatus, int> GetStatusWithAccount(Enums.LoginStatus status, int id)
        {
            return new Tuple<Enums.LoginStatus, int>(status, id);
        }

        public UserViewModel GetUserModel(int accountId)
        {
            return _loginDAO.GetUserModel(accountId);
        }
    }
}
